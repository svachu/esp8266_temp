#include "dallas.hpp"


//Dallas dallas;

Dallas::Dallas() : oneWire (ONE_WIRE_BUS), dallas (&oneWire) {
  Sensor::name = "temperature";
  Sensor::mqttAddress = "";
  Sensor::output = true;
  Sensor::type = "temp";
}

void Dallas::init(int order){
  pinMode(TEMP_POWER_PIN, OUTPUT);
  pinMode(TEMP_GROUND_PIN, OUTPUT);
  digitalWrite(TEMP_GROUND_PIN, LOW);
  digitalWrite(TEMP_POWER_PIN, HIGH);

  Sensor::name += order;

  sensorOrder = order;
  found = false;
  discovery();
  if (found){
      dallas.begin();
      dallas.setResolution(addr, TEMPERATURE_PRECISION);
  }
  loopCounter = 0;
}

void Dallas::discovery(){
 int sensors_num=0;
 byte i;
 byte present = 0;
 byte data[12];
 byte foundAddr[8];

 Serial.print("Looking for 1-Wire devices...\n\r");
 while(oneWire.search(foundAddr)) {
   Serial.print("\n\rFound \'1-Wire\' device with address:\n\r");
   for( i = 0; i < 8; i++) {
     Serial.print("0x");
     if (foundAddr[i] < 16) {
       Serial.print('0');
     }
     Serial.print(foundAddr[i], HEX);
     if (i < 7) {
       Serial.print(", ");
     }
     if (sensors_num == sensorOrder){ // only if this is the sensor we want
       addr[i] = foundAddr[i];
       found = true;
     }
     // addresses[sensors_num][i] = addr[i];
   }

   sensors_num++;
   // if (sensors_num>sensorOrder) return; // we have what we need
   if ( OneWire::crc8( foundAddr, 7) != foundAddr[7]) {
       Serial.print("CRC is not valid!\n");
       return;
   }
 }
 Serial.print("\n\r\n\rThat's it.\r\n");
 Serial.print("Found - ");
 Serial.println(sensors_num);

 oneWire.reset_search();
 return;
}

void Dallas::loop(){
  measure();
  loopCounter++;
}

void Dallas::measure(){
  if (found){
      dallas.requestTemperatures();
      delayMicroseconds(700);
      Sensor::data = dallas.getTempC(addr);
      printState();
  } else {
    Sensor::data = 0;
  }
  Sensor::timestamp = "";

}

void Dallas::printState(){
  Serial.print(addr[0], HEX);
  Serial.print(addr[1], HEX);
  Serial.print(addr[2], HEX);
  Serial.print(addr[3], HEX);
  Serial.print(addr[4], HEX);
  Serial.print(addr[5], HEX);
  Serial.print(addr[6], HEX);
  Serial.print(addr[7], HEX);
  Serial.print(":");
  Serial.println(Sensor::data);
}
