
#include <ESP8266WiFi.h>
extern "C" {
  #include <espnow.h>
}

#include "dallas.hpp"

// this is the MAC Address of the remote ESP server which receives these sensor readings
// uint8_t remoteMac[] = {0x5E, 0xCF, 0x7F, 0x27, 0xB6, 0x8C};
uint8_t remoteMac[] = {0x5E, 0xCF, 0x7F, 0x27, 0xB6, 0x8A}; // base addr for svachu


#define WIFI_CHANNEL 1
#define SLEEP_SECS 15 * 60 // 15 minutes
#define SEND_TIMEOUT 245  // 245 millis seconds timeout 
#define BME_POWER 14

// keep in sync with slave struct
struct __attribute__((packed)) SENSOR_DATA {
  char id[10];
  float temp;
  float pressure;
  float voltage;
  float illu;
  float humidity;
} sensorData;


volatile boolean callbackCalled;

void gotoSleep();
void readSensor();


//ADC_MODE(ADC_VCC); //vcc read

void setup() {
  Serial.begin(115200);
  Serial.println("Booting");
  
  readSensor();

  WiFi.mode(WIFI_STA); // Station mode for esp-now sensor node
  WiFi.disconnect();

  Serial.printf("This mac: %s, ", WiFi.macAddress().c_str()); 
  Serial.printf("target mac: %02x%02x%02x%02x%02x%02x", remoteMac[0], remoteMac[1], remoteMac[2], remoteMac[3], remoteMac[4], remoteMac[5]); 
  Serial.printf(", channel: %i\n", WIFI_CHANNEL); 

  if (esp_now_init() != 0) {
    Serial.println("*** ESP_Now init failed");
    gotoSleep();
  }

  esp_now_set_self_role(ESP_NOW_ROLE_CONTROLLER);
  esp_now_add_peer(remoteMac, ESP_NOW_ROLE_SLAVE, WIFI_CHANNEL, NULL, 0);

  esp_now_register_send_cb([](uint8_t* mac, uint8_t sendStatus) {
    Serial.printf("send_cb, send done, status = %i\n", sendStatus);
    callbackCalled = true;
  });

  callbackCalled = false;

  uint8_t bs[sizeof(sensorData)];
  memcpy(bs, &sensorData, sizeof(sensorData));
  esp_now_send(NULL, bs, sizeof(sensorData)); // NULL means send to all peers

}


void loopthat(){
  // Serial.println(ESP.getVcc());
}


void loop() {
  if (callbackCalled || (millis() > SEND_TIMEOUT)) {
    gotoSleep();
  }
}

void readSensor() {
  
  Dallas * dallas1 = new Dallas();
  //Dallas * dallas2 = new Dallas();

  dallas1->init(0);
  //dallas2->init(1);

  dallas1->loop();
  //dallas2->loop();


  sensorData.temp = dallas1->data;
  sensorData.pressure = 0;
  sensorData.humidity = 0;
  double voltage = analogRead(A0) * (3.3 / 1024.0);
  sensorData.voltage = voltage*1.434782;
  // sensorData.voltage = ((float)analogRead(A0)/1024.0)-0.024;
  String name = "$33"; // cellar
  name.toCharArray(sensorData.id, 10, 0);
  Serial.printf("temp=%01f, humidity=%01f, pressure=%01f, analog=%f\n", sensorData.temp, sensorData.humidity, sensorData.pressure, voltage);
  Serial.println(analogRead(A0));
}

void gotoSleep() {
  // add some randomness to avoid collisions with multiple devices
  int sleepSecs = SLEEP_SECS + ((uint8_t)RANDOM_REG32/2); 

  // sleepSecs = 60;
  Serial.printf("Up for %i ms, going to sleep for %i secs...\n", millis(), sleepSecs); 
  ESP.deepSleep(sleepSecs * 1000000, RF_NO_CAL);
  //delay(60*1000);
  //ESP.restart();
}
