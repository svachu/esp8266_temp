
#ifndef DALLAS_HPP_
#define DALLAS_HPP_

#include <ESP8266WiFi.h>
#include <DallasTemperature.h>
#include <OneWire.h>
#include "sensor.hpp"


#define TEMPERATURE_PRECISION 12
#define TEMP_POWER_PIN  D3 //D5
#define ONE_WIRE_BUS    D4 //D6
#define TEMP_GROUND_PIN D1 //D7


class Dallas : public Sensor {
  public:
    Dallas();
    void init(int order);
    void loop();
    void measure();

  private:
    void discovery();
    void printState();
    int sensorOrder;
    DeviceAddress addr;
    bool found;
    OneWire oneWire;
    DallasTemperature dallas;
    uint8_t loopCounter;
    DeviceAddress addresses[10];
};

//extern Dallas dallas;

#endif
