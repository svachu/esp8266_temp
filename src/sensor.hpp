#ifndef SENSOR_HPP_
#define SENSOR_HPP_

#include <ESP8266WiFi.h>

class Sensor {
  public:
    Sensor();
    void init();
    virtual void loop();

    String name;
    String mqttAddress;
    String type;
    bool   output;
    double data;
    String timestamp;

    virtual void measure();

  private:

};

#endif
